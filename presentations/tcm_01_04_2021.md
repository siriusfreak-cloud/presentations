---
title: "Лекция 11. Дерево разбора. Лемма о разрастании. Алгоритм Кока-Янгера-Касами. Автоматы с магазинной памятью"
block: "Теоретические модели вычислений"
order: 11

---

## Теоретические модели вычислений. 
## Лекция 11. Дерево разбора. Лемма о разрастании. Алгоритм Кока-Янгера-Касами. Автоматы с магазинной памятью.

#### 1 апреля 2021 года

---

### План занятия

1. Деревья разбора <!-- .element: class="fragment" -->
2. Лемма о разрастании <!-- .element: class="fragment" -->
3. Алгоритм Кока-Янгера-Касами <!-- .element: class="fragment" -->
4. Автоматы с магазинной памятью <!-- .element: class="fragment" -->

---

### Нормальная форма Хомского

Грамматикой в нормальной форме Хомского называется КС-грамматика, в которой содержатся правила только следующих видов:

* $A→BC$
* $A→a$
* $S→\lambda$

где $a$ — терминал, $A,B,C$ — нетерминалы, $S$ — стартовая вершина, $\lambda$ — пустая строка, стартовый нетерминал не содержится в правых частях правил.

---

### Нормальная форма Хомского

Теорема: Любую контекстно-свободную грамматику можно привести к нормальной форме Хомского.

Рассматорим КС-грамматику $\Gamma$ и применем к ней последовательно алгоритмы:
1. Добавить новый стартвый нетерминал <!-- .element: class="fragment" -->
2. Удалить $\lambda$-правила <!-- .element: class="fragment" -->
3. Удалить цепные правила <!-- .element: class="fragment" -->
4. Удалить правила длины $>= 2$ с терминалами <!-- .element: class="fragment" -->
5. Удалить правила длины $>= 3$ с нетерминалами <!-- .element: class="fragment" -->
6. Удалим бесполезные символы <!-- .element: class="fragment" -->

Пункты 1 означает добавления правила $S' \rightarrow S$.<!-- .element: class="fragment" -->

Для пункта 4, для всех правил вида $A→u_1u_2$ (где $u_i$ — терминал или нетерминал) заменим все терминалы $u_i$ на новые нетерминалы $U_i$ и добавим правила  $U_i→u_i$.<!-- .element: class="fragment" -->

---

### Дерево разбора КС-грамматик

Деревом разбора грамматики называется дерево, в вершинах которого записаны терминалы или нетерминалы. 

Все вершины, помеченные терминалами, являются листьями. Все вершины, помеченные нетерминалами, имеют детей. <!-- .element: class="fragment" -->

Дети вершины, в которой записан нетерминал, соответствуют раскрытию нетерминала по одному любому правилу (в левой части которого стоит этот нетерминал) и упорядочены так же, как в правой части этого правила. <!-- .element: class="fragment" -->

---

### Дерево разбора КС-грамматик

Для разбора правильной скобочной последовательности: $(()(()))()$:

<img src="/images/tcm_18_03_2021/BracketsSequenceParsingTree1.png" />

---

### Лемма о разрастании для КС-грамматик

Пусть $L$ — КС-грамматика над алфавитом $\Sigma$, тогда существует такое $n$, что для любого слова $ \omega \in L$ длины не меньше $n$ найдутся слова $ u,v,x,y,z \in \Sigma^*$, для которых верно: $uvxyz=\omega, vy\neq \varepsilon, |vxy|\leqslant n$ и $\forall k \geqslant 0~uv^{k}xy^{k}z\in L$.

Условие леммы не является достаточным для контекстно-свободности языка.<!-- .element: class="fragment" -->

Но, в силу необходимости условия, данная лемма часто используется для доказательства неконтекстно-свободности языков.<!-- .element: class="fragment" -->

---

### Лемма о разрастании для КС-грамматик

Грамматика любого контекстно-свободного языка может быть записана в НФХ. Пусть $m$ — количество нетерминалов в грамматике языка $L$, записанной в НФХ.<!-- .element: class="fragment" -->

Выберем $n=2^{m+1}$. Построим дерево разбора произвольного слова $\omega$ длиной больше, чем $n$. <!-- .element: class="fragment" -->



---

### Лемма о разрастании для КС-грамматик

Высотой дерева разбора назовем максимальное число нетерминальных символов на пути от корня дерева к листу. 

Так как грамматика языка $L$ записана в НФХ, то у любого нетерминала в дереве могут быть, либо два потомка нетерминала, либо один потомок терминал. Поэтому  высота дерева разбора слова $\omega$ не меньше $m+1$.

<img src="/images/tcm_18_03_2021/CS_lemma_conspect.png"/>

---

### Лемма о разрастании для КС-грамматик

Выберем путь максимальной длины от корня дерева к листу. 

Количество нетерминалов в нем не меньше, чем $m+1$, следовательно, найдется такой нетерминал $A$, который встречается на этом пути дважды. <!-- .element: class="fragment" -->

Значит, в дереве разбора найдется нетерминал $A$, в поддереве которого содержится нетерминал  $A$. <!-- .element: class="fragment" -->

Выберем такой нетерминал $A$, чтобы в его поддереве содержался такой же нетерминал и длина пути от него до корня была максимальна среди всех нетерминалов, содержащих в поддереве такой же нетерминал. <!-- .element: class="fragment" -->

---

### Лемма о разрастании для КС-грамматик

Найдем слова $ u,v,x,y,z $.

Рассмотрим нетерминал $A$, содержащийся в поддереве выбранного нетерминала. <!-- .element: class="fragment" -->

Тогда $x$ - строка терминалов, которая выведена из рассмотренного нетерминала  в данном дереве разбора. <!-- .element: class="fragment" -->
Тогда $A \Rightarrow^{\ast} x$.<!-- .element: class="fragment" -->

Рассмотрим выбранный ранее нетерминал $A$.<!-- .element: class="fragment" -->

Пусть $t$ - cтрока терминальных символов, которая выведена из рассмотренного нетерминала в данном дереве разбора.<!-- .element: class="fragment" -->

Тогда, так как выбранный нетерминал $A$ содержит в своем поддереве такой же нетерминал, то $A \Rightarrow^{\ast}\alpha A \beta \Rightarrow^{\ast} t$, где $\alpha,\beta$ - строки, которые могут содержать как терминалы, так и нетерминалы.<!-- .element: class="fragment" --> 

---

### Лемма о разрастании для КС-грамматик

При этом как минимум одна из строк $\alpha,\beta$ не пуста, так как грамматика языка записана в НФХ. Пусть $v$ и $y$ - строки, состоящие из терминалов, которые выведены  соответственно из $\alpha$ и $\beta$, в данном дереве разбора. <!-- .element: class="fragment" -->

Тогда $t = vxy$. Так как хотя бы одна из строк $\alpha,\beta$ не пуста, то $vy\neq \varepsilon$.<!-- .element: class="fragment" -->

Получаем $A \Rightarrow^{\ast} vAy \Rightarrow^{\ast} vxy$.<!-- .element: class="fragment" -->

---

### Лемма о разрастании для КС-грамматик

Рассмотрим стартовый нетерминал $S$. Из $S$ выведена строка $\omega$. При этом $S \Rightarrow^{\ast} \alpha A \beta \Rightarrow^{\ast} \omega $, где $A$ -выбранный ранее нетерминал. 

Из $A$ в данном дереве разбора выведена строка $vxy$. <!-- .element: class="fragment" -->

Пусть $u$ и $z$ -строки, состоящие из терминалов, которые выведены соответственно из $\alpha$ и $\beta$ в данном дереве разбора. <!-- .element: class="fragment" -->

Тогда $S \Rightarrow^{\ast} uAz \Rightarrow^{\ast} uvAyz \Rightarrow^{\ast} \omega$.<!-- .element: class="fragment" -->

---

### Лемма о разрастании для КС-грамматик

Покажем, что $|vxy| \leqslant n$. 

Допустим, что $|vxy|>n$.<!-- .element: class="fragment" -->

Тогда высота поддерева с корнем в вершине, соответствующей выбранному $A$, не меньше $m+2$. <!-- .element: class="fragment" -->

Рассмотрим поддерево вершины, в котором содержится нетерминал $A$. <!-- .element: class="fragment" -->

Тогда высота этого поддерева не меньше $m+1$. <!-- .element: class="fragment" -->

Рассмотрим путь максимальной длины от корня этого поддерева к листу. <!-- .element: class="fragment" -->

В нем содержится не менее $m+1$ нетерминалов, причем не содержится стартовый нетерминал. <!-- .element: class="fragment" -->

---

### Лемма о разрастании для КС-грамматик

Следовательно, на этом пути найдутся два одинаковых нетерминала, что противоречит условию наибольшей удаленности от корня выбранного ранее нетерминала $A$. 

Получили противоречие.<!-- .element: class="fragment" -->

Поэтому $|vxy|\leqslant n$.<!-- .element: class="fragment" -->

Таким образом, в рамках нашей грамматики мы можем построить цепочку вывода: 
$$S \Rightarrow^{* } uAz \Rightarrow^{\ast} uvAyz \Rightarrow^{* } uvvAyyz \Rightarrow^{* } uv^{k}Ay^{k}z \Rightarrow^{* } uv^{k}xy^{k}z$$.<!-- .element: class="fragment" -->

Ч.т.д.<!-- .element: class="fragment" -->

---

### Лемма о разрастании для КС-грамматик

Лемма о разрастании является НЕОБХОДИМЫМ условием контекстной свободности языка. Но она не является ДОСТАТОЧНЫМ условием.

Т.е. при помощи леммы о разрастании можно доказывать только не контекстно-свободность языка. <!-- .element: class="fragment" -->

1. Фиксируем некоторое $p$ для языка. <!-- .element: class="fragment" -->
2. Рассматриваем все слова $|w| \ge p$. <!-- .element: class="fragment" -->
3. Находим такие (их может быть несколько) $u,v,x,y,z \in \Sigma^*$, для которых верно: $uvxyz=\omega, vy\neq \varepsilon, |vxy|\leqslant n$ <!-- .element: class="fragment" -->
4. Доказываем, что для каждого найденного разбиения есть такие  $k\geqslant 0$, что  $uv^{k}xy^{k}z\notin L$. <!-- .element: class="fragment" -->

---

### Лемма о разрастании для КС-грамматик

Рассмотрим язык $0^{n}1^{n}2^{n}$. Покажем, что он не является контекстно-свободным.

Для фиксированного $n$ рассмотрим слово $\omega=0^n 1^n 2^n$. <!-- .element: class="fragment" -->

Пусть $\omega$ разбили на $u, v, x, y, z$ произвольным образом. <!-- .element: class="fragment" -->

Так как $|vxy|\leqslant n$, то в слове  $vxy$ не содержится либо ни одного символа $0$, либо ни одного символа $2$. <!-- .element: class="fragment" -->

Для любого такого разбиения выбираем $k=2$ и получаем, что количество символов $1$ изменилось, а количество либо $0$, либо $2$ осталось тем же. <!-- .element: class="fragment" -->

Очевидно, что такое слово не принадлежит рассмотренному языку. <!-- .element: class="fragment" -->

Значит, язык $0^{n}1^{n}2^{n}$ не является контекстно-свободным по лемме о разрастании для КС-грамматик.<!-- .element: class="fragment" -->


---

### Алгоритм Кока-Янгера-Касами 

Пусть дана контекстно-свободная грамматика $Γ$ в нормальной форме Хомского и слово $w∈Σ^\ast$. Требуется выяснить, выводится ли это слово в данной грамматике.

Алгоритм Кока-Янгера-Касами (англ. Cocke-Younger-Kasami algorithm, англ. CYK-алгоритм) — алгоритм, позволяющий по слову узнать, выводимо ли оно в заданной КС-грамматике в нормальной форме Хомского.  <!-- .element: class="fragment" -->
 
Любую КС-грамматику можно привести к НФХ, поэтому алгоритм является универсальным для любой КС-грамматики. <!-- .element: class="fragment" -->

---

### Алгоритм Кока-Янгера-Касами 

Будем решать задачу динамическим программированием. 

Дана строка $w$ размером $n$.  <!-- .element: class="fragment" -->

Заведем для неё трехмерный массив $d$ размером $|N|×n×n$, состоящий из логических значений. <!-- .element: class="fragment" -->

$d[A][i][j]=true$ тогда и только тогда, когда из нетерминала $A$ правилами грамматики можно вывести подстроку $w[i…j]$. <!-- .element: class="fragment" -->

---

### Алгоритм Кока-Янгера-Касами 

Рассмотрим все пары $\lbrace \langle j, i \rangle | j-i=m \rbrace$, где $m$ -- константа и $m < n$. 

* $i = j$. Инициализируем массив для всех нетерминалов, из которых выводится какой-либо символ строки $w$. В таком случае $d[A][i][i] = true \ $, если в грамматике $\Gamma$ присутствует правило $A \rightarrow w[i]$. Иначе $d[A][i][i] = false$. <!-- .element: class="fragment" -->

* $i \ne j$. Значения для всех нетерминалов и пар $\lbrace \langle j', i' \rangle | j' - i' < m \rbrace$ уже вычислены, поэтому $d[A][i][j] = \bigvee\limits_{A \rightarrow BC}\bigvee\limits_{k = i}^{j-1} d[B][i][k] \wedge d[C][k+1][j] \ \ $. То есть, подстроку $w[i \ldots j]$ можно вывести из нетерминала $A$, если существует продукция вида $A \rightarrow BC$ и такое $k$, что подстрока $w[i \ldots k]$ выводима из $B$, а подстрока $w[k + 1 \ldots j]$ выводится из $C$. <!-- .element: class="fragment" -->

После окончания работы значение $d[S][1][n]$ содержит ответ на вопрос, выводима ли данная строка в данной грамматике, где $S$ - начальный символ грамматики. <!-- .element: class="fragment" -->

---

### Алгоритм Кока-Янгера-Касами 

Для определения количества способов вывести слово:

$$ d[A][i][j] = \sum\limits_{A \rightarrow BC}\sum\limits_{k = i}^{j-1} d[B][i][k] \cdot d[C][k + 1][j] \ \ $$ <!-- .element: class="fragment" -->

Минимальная стоимость вывода слова. Пусть $H(A \rightarrow BC)$ стоимость вывода по правилу $A \rightarrow BC$, тогда: <!-- .element: class="fragment" -->

$$ d[A][i][j] = \min\limits_{A \rightarrow BC} \min\limits_{k = i}^{j-1}  ( d[B][i][k] + d[C][k + 1][j] + H(A \rightarrow BC) ) \ \ $$ <!-- .element: class="fragment" -->
 

---

### Алгоритм Кока-Янгера-Касами 

Дана грамматика правильных скобочных последовательностей $\Gamma$ в нормальной форме Хомского.

$$
\begin{array}{l l}   
 A \rightarrow \varepsilon\ |\ BB\ |\ CD\\\\
 B \rightarrow BB\ |\ CD\\\\
 C \rightarrow (\\\\
 D \rightarrow BE\ |\ )\\\\
 E \rightarrow )\\\\
\end{array}
$$

Дано слово $w = ()(())$. Требуется определить можно ли вывести это слово в грамматике $\Gamma$.

---

### Алгоритм Кока-Янгера-Касами (Итерация $m$ = 0)
<div style="font-size: 50%;">

<div id="left">

$A$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>

$B$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


</div>

<div id="right">

$C$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


$D$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>

</div>

<div>
$E$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>
</div>


</div>

---

### Алгоритм Кока-Янгера-Касами (Итерация $m$ = 1)
<div style="font-size: 50%;">

<div id="left">

$A$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>

$B$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


</div>

<div id="right">

$C$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td>●</td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td>●</td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td>●</td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


$D$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
</tbody>
</table>

</div>

$E$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
</tbody>
</table>


</div>

---

### Алгоритм Кока-Янгера-Касами (Итерация $m$ = 2)
<div style="font-size: 50%;">

<div id="left">

$A$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>

$B$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


</div>

<div id="right">

$C$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td>●</td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td>●</td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td>●</td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


$D$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
</tbody>
</table>

</div>


$E$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td>●</td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
</tbody>
</table>


</div>

---

### Алгоритм Кока-Янгера-Касами (Итерация $m$=3)
<div style="font-size: 50%;">

<div id="left">

$A$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>

$B$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


</div>

<div id="right">

$C$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td>●</td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td>●</td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td>●</td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


$D$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
</tbody>
</table>

</div>

$E$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td>●</td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
</tbody>
</table>


</div>

---

### Алгоритм Кока-Янгера-Касами (Итерация $m$=4)
<div style="font-size: 50%;">

<div id="left">

$A$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>

$B$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


</div>

<div id="right">

$C$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td>●</td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td>●</td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td>●</td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


$D$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
</tbody>
</table>

</div>

$E$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td>●</td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
</tbody>
</table>


</div>

---

### Алгоритм Кока-Янгера-Касами (Итерация $m$=5)
<div style="font-size: 50%;">

<div id="left">

$A$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td>●</td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>

$B$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td>●</td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


</div>

<div id="right">

$C$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td>●</td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td>●</td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td>●</td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
</tbody>
</table>


$D$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td> </td><td>●</td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
</tbody>
</table>

</div>

$E$:
<table border="1px">
<thead>
<th></th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th>
</thead>
<tbody>
<tr><td>1</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>2</td><td>●</td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>3</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>4</td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td></tr>
<tr><td>5</td><td> </td><td> </td><td> </td><td> </td><td>●</td><td> </td></tr>
<tr><td>6</td><td> </td><td> </td><td> </td><td> </td><td> </td><td>●</td></tr>
</tbody>
</table>


</div>

---

### Автоматы с магазинной памятью

Автомат с магазинной памятью (автомат со стеком, англ. pushdown automaton) -- это набор $A=\langle\Sigma,\Gamma,Q,s\in Q, T \subset Q, z_0 \in \Gamma, \delta : Q \times \Sigma \cup \{\varepsilon\} \times \Gamma \rightarrow 2^{Q \times \Gamma^*}\rangle$, где:
* $\Sigma$ -- входной алфавит на ленте, <!-- .element: class="fragment" -->
* $\Gamma$ -- стековый алфавит, <!-- .element: class="fragment" -->
* $Q$ -- множество состояний автомата, <!-- .element: class="fragment" -->
* $s$ -- стартовое состояние автомата, <!-- .element: class="fragment" -->
* $T$ -- множество допускающих состояний автомата, <!-- .element: class="fragment" -->
* $z_0$ -- маркер дна стека, <!-- .element: class="fragment" -->
* $\delta$ -- функция переходов. <!-- .element: class="fragment" -->

---

### Автоматы с магазинной памятью

С ленты последовательно считываются символы входного алфавита ($c_i$ — текущий считываемый символ). Символ $x$ снимается с вершины стека. 

Вместо него помещается строка $α$ таким образом, чтобы первый символ строки находился на вершине стека. <!-- .element: class="fragment" -->

<img src="/images/tcm_01_04_2021/PDAsmall.jpeg"></img> <!-- .element: class="fragment" -->

---

### Автоматы с магазинной памятью

Переход: $с$ — символ, прочитанный с ленты; $A$ — символ, вынутый из стека; $α$ — строка, помещаемая в стек.

<img src="/images/tcm_01_04_2021/Transition1.png"></img>

---

### Автоматы с магазинной памятью

Переход: $с$ — символ, прочитанный с ленты; $A$ — символ, вынутый из стека; $α$ — строка, помещаемая в стек.

<img src="/images/tcm_01_04_2021/Transition1.png"></img>


---

### Автоматы с магазинной памятью

Переход: по любому стековому символу, он же возвращается в стек.

<img src="/images/tcm_01_04_2021/Transition2.png"></img>


---

### Автоматы с магазинной памятью

Переход: по любому стековому символу, в стек кладется пустая строка.

<img src="/images/tcm_01_04_2021/Transition3.png"></img>

---

### Автоматы с магазинной памятью

Мгновенное описание (англ. instantaneous descriptions) — это набор $⟨q,α,γ⟩$, где $q$ — текущее состояние, $α$ — остаток строки, $γ$ — содержимое стека.

Переход за один шаг (англ. the "goes-to" relation) обозначается как $⟨q,α,γ⟩⊢⟨r,β,ξ⟩$, где $α=cβ$ (возможно, $c=\lambda$), $γ=χγ′$,$ξ=ηγ′$, $⟨r,η⟩∈δ(q,c,χ)$.

---

### Автоматы с магазинной памятью

Недетерминированный МП-автомат для языка $0^n1^n$.

<img src="/images/tcm_01_04_2021/PDAexample.png"></img>

---

### Автоматы с магазинной памятью

Пусть $P=⟨Q,Σ,Γ,δ,s,Z_0,T⟩$ — МП-автомат. Тогда языком, допускаемым автоматом P по заключительному состоянию, является $L( P)={w∣(s,w,Z_0)⊢^∗(q,ε,α)}$ для некоторого состояния $q∈T$ и произвольной магазинной цепочки $α$.

Начиная с стартовой вершины $s$ и с $w$ на входе, автомат $P$ прочитывает слово $w$ и достигает допускающего состояния. Содержимое магазина в этот момент не имеет значения.

---

### Автоматы с магазинной памятью

Пусть $P=⟨Q,Σ,Γ,δ,s,Z_0,T⟩$ — МП-автомат.  

Jпределим множество допускаемых по пустому магазину слов как $N(P)={w∣(s,w,Z0)⊢^∗(q,ε,ε)}$, где $q$ — произвольное состояние. Таким образом, автомат $P$ прочитывает слово $w$, полностью опустошив свой магазин. Множество заключительных состояний $T$ не имеет значения.

Теорема: Классы языков, допускаемых МП-автоматами по заключительному состоянию и по пустому магазину (стеку), совпадают.

---

### Автоматы с магазинной памятью

Теорема: Множество языков, допускаемых МП-автоматами, совпадает с множеством контекстно-свободных языков.

Теорема доказывается по построению. 


---

### Автоматы с магазинной памятью

Детерминированным автоматом с магазинной памятью (англ. deterministic pushdown automaton) называется автомат с магазинной памятью, для которого выполнены следующие условия:
* $q∈Q,a∈Σ∪{ε},X∈Γ⇒δ(q,a,X)$ имеет не более одного элемента — $δ:Q×Σ∪{ε}×Γ→Q×Γ^∗$.
* Если $δ(q,a,X)$ непусто для некоторого $a∈Σ$, то $δ(q,ε,X)$ должно быть пустым.

В отличие от конечных автоматов, для МП-автоматов недетерминизм является существенным. ДМП-автоматы распознают не все языки, распознаваемые МП-автоматами или КС-грамматиками.


---

### Доказательтсво контекстно-свободности языка

Есть два способа доказать, что язык является контекстно-свободным:

1. Построить грамматику, удовлетворяющую определению КС-языка.
2. Построить автомат с магазинной памятью, допускающий этот язык.


