const d3 = require('d3-graphviz');

// module.exports = () => {
// 	return {
// 		id: 'RevealGraphviz',

// 		init: ( deck ) => {
// 			let graphs = deck.getSlidesElement().getElementsByClassName("graph")
// 			for (var i=0; i< graphs.length; i++) {
// 				let text = graphs[i].textContent;
// 				graphs[i].textContent = "";
// 				d3.graphviz(graphs[i]).renderDot(text);
// 			}
// 		  }
// 		}
// 	}


var RevealGraphviz = (function(){

	return {
		id: 'RevealGraphviz',
		init: function() {
			window.addEventListener("load", function(event) {

				let graphs = document.getElementsByClassName("graph")
				for (var i=0; i< graphs.length; i++) {
					
					let text = graphs[i].textContent;
					console.log(text)
					graphs[i].textContent = "";
					console.log(d3.graphviz(graphs[i]).renderDot(text));
				}
			});
		}
	}

})();

Reveal.registerPlugin( 'RevealGraphviz', RevealGraphviz );
